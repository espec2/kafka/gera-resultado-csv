package com.br.auditoriafinanceira.geraresultadocsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeraResultadoCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeraResultadoCsvApplication.class, args);
	}

}
