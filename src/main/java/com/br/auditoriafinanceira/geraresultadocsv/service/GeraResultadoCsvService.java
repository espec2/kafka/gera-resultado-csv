package com.br.auditoriafinanceira.geraresultadocsv.service;

import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class GeraResultadoCsvService {
    private String pathToCsv = "/home/a2/Documentos/Itau/ValidaCapital-KafkaExec.csv";

    public void gerarCsv(Empresa empresa) throws IOException {
        File csvFile = new File(pathToCsv);
        List<String[]> registrosCsv = new ArrayList<String[]>();

        if (csvFile.isFile()) {
            registrosCsv = lerCsv();
        }else{
            registrosCsv.add(new String[] { "Nome" , "CNPJ" , "StatusCadastro" });
        }
        registrosCsv.add(new String[] { empresa.getNome(), empresa.getCnpj(), empresa.getStatusCadastro().toString()});
        escreverCsv(registrosCsv);
    }

    private List<String[]> lerCsv() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
        String row;
        List<String[]> data = new ArrayList<String[]>();
        while ((row = csvReader.readLine()) != null) {
            data.add(row.split(";"));
        }
        csvReader.close();
        return data;
    }

    private void escreverCsv(List<String[]> registrosCsv){
        File file = new File(pathToCsv);

        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter with '|' as separator
            CSVWriter writer = new CSVWriter(outputfile, ';',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            writer.writeAll(registrosCsv);

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
