package com.br.auditoriafinanceira.geraresultadocsv;

import com.br.auditoriafinanceira.cadastrocnpj.model.Empresa;
import com.br.auditoriafinanceira.geraresultadocsv.service.GeraResultadoCsvService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class GeraResultadoCsvConsumer {
    @Autowired
    private GeraResultadoCsvService geraResultadoCsvService;

    @KafkaListener(topics = "spec2-andre-vinicius-3", groupId = "ValidarCnpj-1")
    public void receber(@Payload Empresa empresa) throws IOException {
        geraResultadoCsvService.gerarCsv(empresa);
        System.out.println(empresa.getCnpj() + ", " + empresa.getNome() + ", " + empresa.getStatusCadastro());
    }
}
